import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class CalculatorTest {
	
	@Disabled
	void testInitialCalculator() {
		calculator c1 = new calculator();
		
		  assertEquals(15,c1.add(4,5,6));


}

 @Test
	void testInitialSingletonCalculator() {
		SingletonCalculator c2 =  SingletonCalculator.getInstance();
		assertEquals(15,c2.add(4,5,6));
	}
}
